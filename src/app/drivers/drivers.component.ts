import { Component, OnInit } from '@angular/core';
import {DriversService} from './drivers.service';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent implements OnInit {

  public driver: any[];
  constructor(private driverService: DriversService) { }
  ngOnInit() {
    this.getDrivers('2018');
  }
  getDrivers(year) {
   this.driverService.getDriversFromAPI(year)
     .subscribe(response => {
       this.driver = response.MRData.DriverTable.Drivers;
       }
     );
  }
}
