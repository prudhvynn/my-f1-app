import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Drivers} from './drivers.model';

@Injectable({
  providedIn: 'root'
})
export class DriversService {
  constructor(private httpClient: HttpClient) { }

  getDriversFromAPI(year): Observable<Drivers> {
    return this.httpClient.get<Drivers>('http://ergast.com/api/f1/'+year+'/drivers.json');
  }
}
